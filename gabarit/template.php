<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $titre ?></title>
        <link rel="stylesheet" href="css/transat.css" />
    </head>

    <body>
        <div id="conteneur">
            <header>
                <div class='login'><?php 
                if (isset($_SESSION['logged_in']['login']) == TRUE) {
                    echo '<span id="salutation">Salut à toi, '.$_SESSION['logged_in']['prenom'].' '.$_SESSION['logged_in']['nom'].'!</span> ';
                    ?><br/><a class="deco" href="logout.php">Se déconnecter</a><?php
                }else{?>
                    <form method="post" action="login_action.php">
                        <fieldset>
                            <legend>Connectez-vous</legend>
                            <input type="text" name="login" value="" placeholder="Login" required>
                            <input type="text" name="password" value="" placeholder="Password" required>
                            <input type="submit" value="Valider">
                        </fieldset>
                    </form><?php } ?>   
                </div>
            </header>

            <nav>
                <ul>
                    <li><a href="index.php" class="navitem">Accueil</a></li>
                    <li><a href="classements.php" class="navitem">Classements</a></li>
                    <li><a href="ajoutclasse.php" class="navitemprivate">Ajout Classe</a></li>
                </ul>
            </nav>
            <section>
                    <?php echo $contenu ; ?>
            </section>
            <footer>
                <p>Copyright Moi - Tous droits réservés - 
                    <a href="#">Contact</a></p>
            </footer>
        </div>    
    </body>
</html>