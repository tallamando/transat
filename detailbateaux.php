<?php 
    session_start();
    require 'bdd/bddconfig.php';

    $idBateau = 0;
    if(isset($_GET['idBateau'])) {
        $idBateau = intval(htmlspecialchars($_GET['idBateau']));
    }

try {
    $objBdd = new PDO("mysql:host=$bddserver; dbname=$bddname; charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $listeDetails = $objBdd->query("SELECT * FROM bateau WHERE idBateau=$idBateau");
    $listeSkipper=  $objBdd->query("SELECT * FROM skipper WHERE idBateau=$idBateau");
    $detail = $listeDetails->fetch();
    $skip = $listeSkipper->fetch();
       } catch (Exception $prmE) { die('Erreur : ' . $prmE->getMessage()); }

$titre = "Détails Bateaux";
ob_start();
?>
            <h1>Détails Bateaux</h1>
            <table><tr>
    <td><img src="images/bateaux/<?= $detail['photo'];?>"></td><td><img src="images/skippers/<?php echo $skip['photo']; ?>"></td></tr>
     <tr><td><span> <?= $detail['nomBateau'];?></span></td><td><span> <?= $skip['nomSkipper'];?></span></td></tr></table>
            

<?php 
$contenu = ob_get_clean();
require 'gabarit/template.php';
?>