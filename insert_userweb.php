<?php

session_start();


$login = strtolower('jacques');
$password_clair = 'vabre';
$nom = 'Vabre'; 
$prenom = 'Jacques';


$hash_password = password_hash($password_clair, PASSWORD_BCRYPT);


require 'bdd/bddconfig.php';
try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $PDOinsertuserweb = $objBdd->prepare("INSERT INTO userweb (login, password, nom, prenom) VALUES (:login, :password, :nom, :prenom)");
        $PDOinsertuserweb->bindParam(':login', $login, PDO::PARAM_STR);
        $PDOinsertuserweb->bindParam(':password', $hash_password, PDO::PARAM_STR);
        $PDOinsertuserweb->bindParam(':nom', $nom, PDO::PARAM_STR);
        $PDOinsertuserweb->bindParam(':prenom', $prenom, PDO::PARAM_STR);
        $PDOinsertuserweb->execute();
        
        echo $lastId = $objBdd->lastInsertId();


} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}