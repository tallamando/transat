<?php
    require "bdd/bddconfig.php";
    session_start();
//récupérer les 3 variables POST
//sécuriser les variables reçues
$paramOK = false;

if(isset($_POST["nomClasse"])) {
    $nomClasse = htmlspecialchars($_POST["nomClasse"]);
    if (isset($_POST["typeCoque"])) {
        $typeCoque = htmlspecialchars($_POST["typeCoque"]);
        if (isset($_POST["tailleCoque"])) {
            $tailleCoque = htmlspecialchars($_POST["tailleCoque"]);
            $paramOK = true;
        }
    }
}
 if ($paramOK == true) {

//INSERT dans la base
    try{
        $objBdd = new PDO ("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
        $pdoStmt = $objBdd->prepare("INSERT INTO classebateau (nomClasse, typeCoque, tailleCoque) VALUES(:nomClasse, :typeCoque, :tailleCoque)");
        $pdoStmt ->bindParam(':nomClasse',$nomClasse, PDO::PARAM_STR);
        $pdoStmt ->bindParam(':typeCoque', $typeCoque, PDO::PARAM_STR);
        $pdoStmt ->bindParam(':tailleCoque', $tailleCoque, PDO::PARAM_STR);
        $pdoStmt ->execute();

        $lastID = $objBdd->lastInsertId();
        echo $lastID;


    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }


    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'index.php';
    header("Location: http://$serveur$chemin/$page");


 }else{
     die("Les paramètres reçus ne sont pas valides.");
 }



?>