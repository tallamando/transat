<link rel="stylesheet" href="style.css">
<?php $titre = "login_action";?>
<?php ob_start(); ?>

<?php
session_start();

$paramOK = false;
if (isset($_POST["login"])) {
    $login = strtolower(htmlspecialchars($_POST["login"]));
    if (isset($_POST["password"])) {
        $password = htmlspecialchars($_POST["password"]);
        $paramOK = true;
    }
}

if ($paramOK == true) {
} else {
    die('Vous devez fournir un login et un mot de passe');
}
require 'bdd/bddconfig.php';
try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $PDOlistlogins = $objBdd->prepare("SELECT * FROM userweb WHERE login = :login ");
    $PDOlistlogins->bindParam(':login', $login, PDO::PARAM_STR);
    $PDOlistlogins->execute();

$row_userweb = $PDOlistlogins->fetch();
if ($row_userweb != false) {
    

} else {
    //Mauvais login
    session_destroy();
    die('Authentification incorrecte');
}

if (password_verify($password, $row_userweb['password'])) {
    
}else {
    
    session_destroy();
    die('Authentification incorrecte');
}
}catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}

$session_data = array(
    'id' => $row_userweb['id'],
    'login' => $row_userweb['login'],
    'nom' => $row_userweb['nom'],
    'prenom' => $row_userweb['prenom']
);

session_regenerate_id();

$_SESSION['logged_in'] = $session_data;
$PDOlistlogins->closeCursor();


$serveur = $_SERVER['HTTP_HOST'];
$chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
$page = 'index.php';
header("Location: http://$serveur$chemin/$page");


?>
<?php $contenu=ob_get_clean()?>
<?php require 'gabarit/template.php'?>