<?php 
    session_start();
    require 'bdd/bddconfig.php';

    $idClasse = 0;
    if(isset($_GET['idClasse'])) {
        $idClasse = intval(htmlspecialchars($_GET['idClasse']));
    }

try {
    $objBdd = new PDO("mysql:host=$bddserver; dbname=$bddname; charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $listeClasses = $objBdd->query("SELECT * FROM classebateau");

    $listeBateaux = $objBdd->prepare("SELECT * FROM bateau");
    /*$listeBateaux->bindParam(':id',$idClasse, PDO::PARAM_INT);*/
    $listeBateaux->execute();
    $bateau = $listeBateaux->fetch();

       } catch (Exception $prmE) { die('Erreur : ' . $prmE->getMessage()); }

$titre = "Classement";
ob_start();
?>
            <h1>Classement</h1>
            <span>
    <?php
    while ($classe = $listeClasses->fetch()) {
    
    echo $classe['typeCoque']; ?>&nbsp;<a href="listebateaux.php?idClasse=<?php echo $classe['idClasse']; ?>"><?php echo $classe['nomClasse']; ?></a><br>
     <?php
    }//fin du while
    $listeClasses->closeCursor(); //libère les ressources de la BDD
    ?>
            </span>

<?php 
$contenu = ob_get_clean();
require 'gabarit/template.php';
?>