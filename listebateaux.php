<?php 
    session_start();
    require 'bdd/bddconfig.php';

    $idClasse = 0;
    if(isset($_GET['idClasse'])) {
        $idClasse = intval(htmlspecialchars($_GET['idClasse']));
    }

try {
    $objBdd = new PDO("mysql:host=$bddserver; dbname=$bddname; charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $listeBateaux = $objBdd->query("SELECT * FROM bateau WHERE idClasse=$idClasse");
    
       } catch (Exception $prmE) { die('Erreur : ' . $prmE->getMessage()); }

$titre = "Liste Bateaux";
ob_start();
?>
            <h1>Liste bateaux</h1>
            <ol>
    <?php
    while ($bateau = $listeBateaux->fetch()) {
    
    ?><li><a href="detailbateaux.php?idBateau=<?= $bateau['idBateau']; ?>"><?php echo $bateau['nomBateau']; ?></a></li>
     <?php
    }?>
            </ol>

<?php 
$contenu = ob_get_clean();
require 'gabarit/template.php';
?>